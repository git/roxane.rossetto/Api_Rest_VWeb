package SAE.ApiREST.WebService.model;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;
    String title;
    String url;
    LocalDate dateAdded;
    LocalDate datePublished;
    Boolean visible;
    Integer type;
    // ArrayList<Keyword> keywords = new ArrayList<>();

    public Article() {}

    public Article(String title, String url, LocalDate dateAdded, LocalDate datePublished, Boolean visibility, Integer type) {
        this.id = 1;
        this.title = title;
        this.url = url;
        this.dateAdded = dateAdded;
        this.datePublished = datePublished;
        this.visible = visibility;
        this.type = type;
    }

    public Article(String title, String url, String dateAdded, String datePublished, Boolean visibility, Integer type) {
        this.id = 1;
        this.title = title;
        this.url = url;
        this.dateAdded = LocalDate.parse(dateAdded, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        this.datePublished = LocalDate.parse(datePublished, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        this.visible = visibility;
        this.type = type;
    }
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public LocalDate getDateAdded() {
        return this.dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = LocalDate.parse(dateAdded, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    public LocalDate getDatePublished() {
        return this.datePublished;
    }

    public void setDatePublished(String datePublished) {
        this.datePublished = LocalDate.parse(datePublished, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    public Boolean getVisible() {
        return this.visible;
    }

    public void setVisibility(Boolean visible) {
        this.visible = visible;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
/*
    public List<Keyword> getKeywords() {
        return this.keywords;
    }

    public void setKeywords(List<Keyword> keywords) {
        this.keywords = keywords;
    }
*/
}
