package SAE.ApiREST.WebService.model;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Collect {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    @Column(name = "articles")
    private ArrayList<Article> articles;
    @Column(name = "name")
    private String name;
    @Column(name = "teacher")
    private Teacher teacher;

    public Collect() {
    }

    public Collect(String name, Teacher teacher){
        this.name = name;
        this.teacher  = teacher;
        this.articles = new ArrayList<Article>();
    }
    public Collect(String name, Teacher teacher, Integer id){
        this.name = name;
        this.teacher  = teacher;
        this.articles = new ArrayList<Article>();
        this.id = id;
    }

    // region Article
    public Integer getId(){
        return id;
    }
    // endregion

    // region Article
    public List<Article> getAllArticles(){
        return articles;
    }

    // region addArticle
    public void addArticle(Article article){
        if(!this.articles.contains(article)){
            this.articles.add(article);
        }
    }
    public void addArticles(List<Article> articles){
        for(Article article : articles){
            addArticle(article);
        }
    }
    // endregion

    // region removeArticle
    public void removeArticle(Article article){
        this.articles.remove(article);
    }
    public void removeArticles(List<Article> articles){
        this.articles.removeAll(articles);
    }
    // endregion
    // endregion

    // region name
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    // endregion

    // region teacher
    public Teacher getTeacher(){
        return teacher;
    }
    // endregion
}