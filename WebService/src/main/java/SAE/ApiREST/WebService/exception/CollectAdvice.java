package SAE.ApiREST.WebService.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class CollectAdvice {
    @ResponseBody
    @ExceptionHandler(CollectException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String collectHandler(CollectException ex) {
        return ex.getMessage();
    }
}