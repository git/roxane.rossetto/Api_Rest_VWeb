package SAE.ApiREST.WebService.exception;

public class TeacherException extends RuntimeException {
    public TeacherException(String exception) {
        super(exception);
    }
}
