package SAE.ApiREST.WebService.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ControllerAdvice
public class ArticleAdvice {
    @ResponseBody
    @ExceptionHandler(ArticleException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String articleHandler(ArticleException ex) {
        return ex.getMessage();
    }
}
