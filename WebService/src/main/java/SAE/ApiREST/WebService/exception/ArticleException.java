package SAE.ApiREST.WebService.exception;

public class ArticleException extends RuntimeException {
    public ArticleException(String exception) {
        super(exception);
    }
}
