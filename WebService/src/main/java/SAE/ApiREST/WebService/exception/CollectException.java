package SAE.ApiREST.WebService.exception;

public class CollectException extends RuntimeException {
    public CollectException(String exception) {
        super(exception);
    }
}
