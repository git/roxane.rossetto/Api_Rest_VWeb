package SAE.ApiREST.WebService.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import SAE.ApiREST.WebService.Response;
import SAE.ApiREST.WebService.exception.ArticleException;
import SAE.ApiREST.WebService.model.Article;
import SAE.ApiREST.WebService.service.IArticleService;

import java.util.ArrayList;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Controller
@RequestMapping("/ArticleWebService")
public class ArticleControler {
    @Autowired
    IArticleService articleService;

    // region POST
    @PostMapping(
        value = "/addArticle",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
        )
    public @ResponseBody EntityModel<Response> addArticle(
        @RequestBody Article article
    ) {
        Response results = articleService.addArticle(article);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).addArticle(article)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("allArticles"),
            linkTo(methodOn(ArticleControler.class).getArticleById(article.getId())).withRel("getArticleById"),
            linkTo(methodOn(ArticleControler.class).deleteArticleFromId(article.getId())).withRel("deleteArticle")
        );
    }
    // endregion

    // region PUT
    @PutMapping(
        value = "/updateTitle/{title}",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Article> updateTitle(
        @PathVariable("title") String title,
        @RequestBody Article article
    ) {
        Article results = articleService.updateTitle(article, title);
        
        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).updateTitle(title, article)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("allArticles"),
            linkTo(methodOn(ArticleControler.class).getArticleById(article.getId())).withRel("getArticleById"),
            linkTo(methodOn(ArticleControler.class).getArticlesByTitle(article.getTitle())).withRel("getArticlesByTitle"),
            linkTo(methodOn(ArticleControler.class).deleteArticleFromId(article.getId())).withRel("deleteArticle")
        );
    }
    
    @PutMapping(
        value = "/updateUrl/{url}",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Article> updateUrl(
        @PathVariable("url") String url,
        @RequestBody Article article
    ) {
        Article results = articleService.updateUrl(article, url);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).updateUrl(url, article)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("allArticles"),
            linkTo(methodOn(ArticleControler.class).getArticleById(article.getId())).withRel("getArticleById"),
            linkTo(methodOn(ArticleControler.class).getArticlesByUrl(article.getUrl())).withRel("getArticlesByUrl"),
            linkTo(methodOn(ArticleControler.class).deleteArticleFromId(article.getId())).withRel("deleteArticle")
        );
    }
    
    @PutMapping(
        value = "/updateDatePublished/{datePublished}",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Article> updateDatePublished(
        @PathVariable("datePublished") String datePublished,
        @RequestBody Article article
    ) {
        Article results = articleService.updateDatePublished(article, datePublished);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).updateDatePublished(datePublished, article)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("allArticles"),
            linkTo(methodOn(ArticleControler.class).getArticleById(article.getId())).withRel("getArticleById"),
            linkTo(methodOn(ArticleControler.class).getArticlesPublishedBefore(article.getDatePublished().toString())).withRel("getArticlesPublishedBefore"),
            linkTo(methodOn(ArticleControler.class).getArticlesPublishedAfter(article.getDatePublished().toString())).withRel("getArticlesPublishedAfter"),
            linkTo(methodOn(ArticleControler.class).deleteArticleFromId(article.getId())).withRel("deleteArticle")
        );
    }
    
    @PutMapping(
        value = "/updateDateAdded/{dateAdded}",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Article> updateDateAdded(
        @PathVariable("dateAdded") String dateAdded,
        @RequestBody Article article
    ) {
        Article results = articleService.updateDateAdded(article, dateAdded);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).updateDateAdded(dateAdded, article)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("allArticles"),
            linkTo(methodOn(ArticleControler.class).getArticleById(article.getId())).withRel("getArticleById"),
            linkTo(methodOn(ArticleControler.class).getArticlesAddedBefore(article.getDateAdded().toString())).withRel("getArticlesAddedBefore"),
            linkTo(methodOn(ArticleControler.class).getArticlesAddedAfter(article.getDateAdded().toString())).withRel("getArticlesAddedAfter"),
            linkTo(methodOn(ArticleControler.class).deleteArticleFromId(article.getId())).withRel("deleteArticle")
        );
    }
    
    @PutMapping(
        value = "/changeVisibility",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Article> changeVisibility(
        @RequestBody Article article
    ) {
        Article results = articleService.changeVisibility(article);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).changeVisibility(article)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("allArticles"),
            linkTo(methodOn(ArticleControler.class).getArticleById(article.getId())).withRel("getArticleById"),
            linkTo(methodOn(ArticleControler.class).getVisibleArticles()).withRel("getVisibleArticle"),
            linkTo(methodOn(ArticleControler.class).getInvisibleArticles()).withRel("getInvisibleArticle"),
            linkTo(methodOn(ArticleControler.class).deleteArticleFromId(article.getId())).withRel("deleteArticle")
        );
    }
    
    @PutMapping(
        value = "/updateType/{type}",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Article> updateType(
        @PathVariable("type") Integer type,
        @RequestBody Article article
    ) {
        Article results = articleService.updateType(article, type);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).updateType(type, article)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("allArticles"),
            linkTo(methodOn(ArticleControler.class).getArticleById(article.getId())).withRel("getArticleById"),
            linkTo(methodOn(ArticleControler.class).getArticlesByType(article.getType())).withRel("getArticlesByType"),
            linkTo(methodOn(ArticleControler.class).deleteArticleFromId(article.getId())).withRel("deleteArticle")
        );
    }
    // endregion

    // region GET
    @GetMapping(
        value = "/getAllArticle", 
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody CollectionModel<Article> getAllArticles() {
        ArrayList<Article> results = (ArrayList<Article>) articleService.getAllArticles();
        
        if(results.isEmpty()) {
            throw new ArticleException("No articles available");
        }
        
        return CollectionModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withSelfRel()
        );
    }

    @GetMapping(
        value = "/getArticleById/{id}", 
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Article> getArticleById(@PathVariable(value = "id") Integer id) {
        Article results = articleService.getArticlesById(id);
        
        if(results == null) {
            throw new ArticleException("Undefined id");
        }
        
        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getArticleById(id)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).deleteArticleFromId(id)).withRel("deleteArticleFromUrl")
        );
    }

    @GetMapping(
        value = "/getArticlesByTitle/{title}", 
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody CollectionModel<Article> getArticlesByTitle(@PathVariable(value = "title") String title) {
        ArrayList<Article> results = (ArrayList<Article>) articleService.getArticlesByTitle(title);
        
        if(results.isEmpty()) {
            throw new ArticleException("Undefined title");
        }
        
        return CollectionModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getArticlesByTitle(title)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).deleteArticleFromTitle(title)).withRel("deleteArticleFromTitle")
        );
    }

    @GetMapping(
        value = "/getArticlesByUrl/{url}", 
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody CollectionModel<Article> getArticlesByUrl(@PathVariable(value = "url") String url) {
        ArrayList<Article> results = (ArrayList<Article>) articleService.getArticlesByUrl(url);
        
        if(results.isEmpty()) {
            throw new ArticleException("Undefined title");
        }
        
        return CollectionModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getArticlesByUrl(url)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).deleteArticleFromUrl(url)).withRel("deleteArticleFromId")
        );
    }

    @GetMapping(
        value = "/getArticlesByType/{type}", 
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody CollectionModel<Article> getArticlesByType(@PathVariable(value = "type") Integer type) {
        ArrayList<Article> results = (ArrayList<Article>) articleService.getArticlesByType(type);

        if (results.isEmpty()) {
            throw new ArticleException(String.format("No content of type %d", type));
        }
        
        return CollectionModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getArticlesByType(type)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).deleteArticleFromType(type)).withRel("deleteArticleFromType")
        );
    }

    @GetMapping(
        value = "/getVisibleArticles", 
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody CollectionModel<Article> getVisibleArticles() {
        ArrayList<Article> results = (ArrayList<Article>) articleService.getVisibleArticles();

        if (results.isEmpty()) {
            throw new ArticleException("No visible article");
        }
        
        return CollectionModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getVisibleArticles()).withSelfRel()
        );
    }

    @GetMapping(
        value = "/getInvisibleArticles", 
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody CollectionModel<Article> getInvisibleArticles() {
        ArrayList<Article> results = (ArrayList<Article>) articleService.getInvisibleArticles();

        if (results.isEmpty()) {
            throw new ArticleException("No invisible article");
        }
        
        return CollectionModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getInvisibleArticles()).withSelfRel()
        );
    }

    @GetMapping(
        value = "/getArticlesAddedBefore/{dateAdded}", 
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody CollectionModel<Article> getArticlesAddedBefore(@PathVariable(value = "dateAdded") String dateAdded) {
        ArrayList<Article> results = (ArrayList<Article>) articleService.getArticlesAddedBefore(dateAdded);

        if (results.isEmpty()) {
            throw new ArticleException(String.format("No article added before %t", dateAdded));
        }
        
        return CollectionModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getArticlesAddedBefore(dateAdded)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).deleteArticleAddedBefore(dateAdded)).withRel("deleteArticleAddedBefore")
        );
    }

    @GetMapping(
        value = "/getArticlesAddedAfter/{dateAdded}", 
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody CollectionModel<Article> getArticlesAddedAfter(@PathVariable(value = "dateAdded") String dateAdded) {
        ArrayList<Article> results = (ArrayList<Article>) articleService.getArticlesAddedAfter(dateAdded);

        if (results.isEmpty()) {
            throw new ArticleException(String.format("No article added after %t", dateAdded));
        }
        
        return CollectionModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getArticlesAddedAfter(dateAdded)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).deleteArticleAddedAfter(dateAdded)).withRel("deleteArticleAddedAfter")
        );
    }

    @GetMapping(
        value = "/getArticlesAddedBetween/{beginning}/{end}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody CollectionModel<Article> getArticlesAddedBetween(@PathVariable(value = "beginning") String beginning, @PathVariable(value = "end") String end) {
        ArrayList<Article> results = (ArrayList<Article>) articleService.getArticlesAddedBetween(beginning, end);

        if (results.isEmpty()) {
            throw new ArticleException(String.format("No article added between %t and %t", beginning, end));
        }
        
        return CollectionModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getArticlesAddedBetween(beginning, end)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).deleteArticleAddedBetween(beginning, end)).withRel("deleteArticleAddedBetween")
        );
    }

    @GetMapping(
        value = "/getArticlesPublishedBefore/{datePublished}", 
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody CollectionModel<Article> getArticlesPublishedBefore(@PathVariable(value = "datePublished") String datePublished) {
        ArrayList<Article> results = (ArrayList<Article>) articleService.getArticlesPublishedBefore(datePublished);

        if (results.isEmpty()) {
            throw new ArticleException(String.format("No article published before %t", datePublished));
        }
        
        return CollectionModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getArticlesPublishedBefore(datePublished)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).deleteArticlePublishedBefore(datePublished)).withRel("deleteArticlePublishedBefore")
        );
    }

    @GetMapping(
        value = "/getArticlesPublishedAfter/{datePublished}", 
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody CollectionModel<Article> getArticlesPublishedAfter(@PathVariable(value = "datePublished") String datePublished) {
        ArrayList<Article> results = (ArrayList<Article>) articleService.getArticlesPublishedAfter(datePublished);

        if (results.isEmpty()) {
            throw new ArticleException(String.format("No article published after %t", datePublished));
        }
        
        return CollectionModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getArticlesPublishedAfter(datePublished)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).deleteArticlePublishedAfter(datePublished)).withRel("deleteArticlePublishedAfter")
        );
    }

    @GetMapping(
        value = "/getArticlesPublishedBetween/{beginning}/{end}", 
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody CollectionModel<Article> getArticlesPublishedBetween(@PathVariable(value = "beginning") String beginning, @PathVariable(value = "end") String end) {
        ArrayList<Article> results = (ArrayList<Article>) articleService.getArticlesPublishedBetween(beginning, end);

        if (results.isEmpty()) {
            throw new ArticleException(String.format("No article published between %t and %t", beginning, end));
        }
        
        return CollectionModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).getArticlesPublishedBetween(beginning, end)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).deleteArticlePublishedBetween(beginning, end)).withRel("getArticlesPublishedBetween")
        );
    }
    
    // endregion

    // region DELETE
    @DeleteMapping(
        value = "/deleteArticleFromId/{id}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Response> deleteArticleFromId(
        @PathVariable("id") Integer id
    ) {
        Response results = articleService.deleteArticleFromId(id);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).deleteArticleFromId(id)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("getAllArticles"),
            linkTo(methodOn(ArticleControler.class).getArticleById(id)).withRel("getArticleById")
        );
    }

    @DeleteMapping(
        value = "/deleteArticleFromTitle/{title}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Response> deleteArticleFromTitle(
        @PathVariable("title") String title
    ) {
        Response results = articleService.deleteArticleFromTitle(title);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).deleteArticleFromTitle(title)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("getAllArticles"),
            linkTo(methodOn(ArticleControler.class).getArticlesByTitle(title)).withRel("getArticleByTitle")
        );
    }

    @DeleteMapping(
        value = "/deleteArticleFromUrl/{url}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Response> deleteArticleFromUrl(
        @PathVariable("url") String url
    ) {
        Response results = articleService.deleteArticleFromTitle(url);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).deleteArticleFromUrl(url)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("getAllArticles"),
            linkTo(methodOn(ArticleControler.class).getArticlesByUrl(url)).withRel("getArticlesByUrl")
        );
    }

    @DeleteMapping(
        value = "/deleteArticleFromType/{type}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Response> deleteArticleFromType(
        @PathVariable("type") Integer type
    ) {
        Response results = articleService.deleteArticleFromType(type);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).deleteArticleFromType(type)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("getAllArticles"),
            linkTo(methodOn(ArticleControler.class).getArticlesByType(type)).withRel("getArticlesByType")
        );
    }

    @DeleteMapping(
        value = "/deleteArticleAddedBefore/{date}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Response> deleteArticleAddedBefore(
        @PathVariable("date") String date
    ) {
        Response results = articleService.deleteArticleAddedBefore(date);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).deleteArticleAddedBefore(date)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("getAllArticles"),
            linkTo(methodOn(ArticleControler.class).getArticlesAddedBefore(date)).withRel("getArticlesAddedBefore")
        );
    }

    @DeleteMapping(
        value = "/deleteArticleAddedAfter/{date}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Response> deleteArticleAddedAfter(
        @PathVariable("date") String date
    ) {
        Response results = articleService.deleteArticleAddedAfter(date);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).deleteArticleAddedAfter(date)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("getAllArticles"),
            linkTo(methodOn(ArticleControler.class).getArticlesAddedAfter(date)).withRel("getArticlesAddedAfter")
        );
    }
    
    @DeleteMapping(
        value = "/deleteArticleAddedBetween/{beginning}/{end}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Response> deleteArticleAddedBetween(
        @PathVariable("beginning") String beginning,
        @PathVariable("end") String end
    ) {
        Response results = articleService.deleteArticleAddedBetween(beginning, end);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).deleteArticleAddedBetween(beginning, end)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("getAllArticles"),
            linkTo(methodOn(ArticleControler.class).getArticlesAddedBetween(beginning, end)).withRel("getArticlesAddedBetween")
        );
    }

    @DeleteMapping(
        value = "/deleteArticlePublishedBefore/{date}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Response> deleteArticlePublishedBefore(
        @PathVariable("date") String date
    ) {
        Response results = articleService.deleteArticlePublishedBefore(date);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).deleteArticlePublishedBefore(date)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("getAllArticles"),
            linkTo(methodOn(ArticleControler.class).getArticlesPublishedBefore(date)).withRel("getArticlesPublishedBefore")
        );
    }

    @DeleteMapping(
        value = "/deleteArticlePublishedAfter/{date}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Response> deleteArticlePublishedAfter(
        @PathVariable("date") String date
    ) {
        Response results = articleService.deleteArticlePublishedBefore(date);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).deleteArticlePublishedAfter(date)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("getAllArticles"),
            linkTo(methodOn(ArticleControler.class).getArticlesPublishedAfter(date)).withRel("getArticlesPublishedAfter")
        );
    }
    
    @DeleteMapping(
        value = "/deleteArticlePublishedBetween/{beginning}/{end}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody EntityModel<Response> deleteArticlePublishedBetween(
        @PathVariable("beginning") String beginning,
        @PathVariable("end") String end
    ) {
        Response results = articleService.deleteArticlePublishedBetween(beginning, end);

        return EntityModel.of(
            results,
            linkTo(methodOn(ArticleControler.class).deleteArticlePublishedBetween(beginning, end)).withSelfRel(),
            linkTo(methodOn(ArticleControler.class).getAllArticles()).withRel("getAllArticles"),
            linkTo(methodOn(ArticleControler.class).getArticlesPublishedBetween(beginning, end)).withRel("getArticlesPublishedBetween")
        );
    }
    // endregion
}
