package SAE.ApiREST.WebService.controller;

import SAE.ApiREST.WebService.exception.TeacherException;
import SAE.ApiREST.WebService.model.Teacher;
import SAE.ApiREST.WebService.service.ITeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.awt.*;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Controller
@RequestMapping("/ProfWebService")
public class TeacherController {
    @Autowired
    private ITeacherService iTeacherServ;


    public TeacherController(ITeacherService iserv) {
        this.iTeacherServ = iserv;
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody CollectionModel<Teacher> getAllTeacher(){
        return CollectionModel.of(
                iTeacherServ.getAllTeacher(),
                linkTo(methodOn(TeacherController.class).getAllTeacher()).withSelfRel());
    }
    @PostMapping(value = "addTeacher",produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody EntityModel<Teacher> createTeacher( @RequestBody Teacher teach){

        return EntityModel.of(teach,
                linkTo(methodOn(TeacherController.class).createTeacher(teach)).withSelfRel(),
                linkTo(methodOn(TeacherController.class).getAllTeacher()).withRel("all"));
    }
    @GetMapping(value = "/getid/{id}")
    public @ResponseBody EntityModel<Teacher> getTeachById(@PathVariable("id") Integer id){
        Teacher tt = iTeacherServ.getTeacherById(id);
        if( tt == null ){
            throw new TeacherException("No teacher found for this id !");
        }
        return EntityModel.of(tt,
                linkTo(methodOn(TeacherController.class).getTeachById(id)).withSelfRel(),
                linkTo(methodOn(TeacherController.class).getAllTeacher()).withRel("all"));
    }
    @GetMapping(value = "/getusername/{username}")
    public @ResponseBody EntityModel<Teacher> getTeachByUsername(@PathVariable("username") String username) {
        Teacher tt = iTeacherServ.getTeacherByUsername(username);
        if (tt == null) {
            throw new TeacherException("No teacher found for this username");
        }
        return EntityModel.of(tt,
                linkTo(methodOn(TeacherController.class).getTeachByUsername(username)).withSelfRel(),
                linkTo(methodOn(TeacherController.class).getAllTeacher()).withRel("all"));
    }
    @GetMapping( value = "/getmail/{mail}" )
    public @ResponseBody EntityModel<Teacher> getTeachByMail(@PathVariable("mail") String mail) {
        Teacher tt = iTeacherServ.getTeacherByMail(mail);
        if( tt == null ) {
            throw new TeacherException("No teacher found for this mail");
        }
        return EntityModel.of(tt,
                linkTo(methodOn(TeacherController.class).getTeachByMail(mail)).withSelfRel(),
                linkTo(methodOn(TeacherController.class).getAllTeacher()).withRel("all"));
    }
    @GetMapping( value = "/getdate/{date}" )
    public @ResponseBody Teacher getTeachByDate(@PathVariable("date") String date) {
        Teacher tt = iTeacherServ.getTeacherByMail(date);
        if( tt == null ) {
            throw new TeacherException("No teacher found for this mail");
        }
        return  tt;
    }
    @PutMapping( value = "/modify/{username}" )
    public @ResponseBody EntityModel<Teacher> modifyTeachUsername(@PathVariable("username") String username, Teacher tt){
        if( username == "" ){
            throw new TeacherException("Username provided for modification is empty");
        }
        iTeacherServ.modifyUsername(tt,username);
        return EntityModel.of(tt,
                linkTo(methodOn(TeacherController.class).modifyTeachUsername(username,tt)).withSelfRel(),
                linkTo(methodOn(TeacherController.class).getAllTeacher()).withRel("all"));
    }
    @DeleteMapping( value = "delete")
    public @ResponseBody EntityModel<List<Teacher>> deleteTeacher(Integer id){
        return EntityModel.of(iTeacherServ.deleteTeacher(id),
                linkTo(methodOn(TeacherController.class).deleteTeacher(id)).withSelfRel(),
                linkTo(methodOn(TeacherController.class).getAllTeacher()).withRel("all"));
    }
}
