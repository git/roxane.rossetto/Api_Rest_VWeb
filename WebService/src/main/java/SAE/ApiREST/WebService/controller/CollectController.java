package SAE.ApiREST.WebService.controller;

import SAE.ApiREST.WebService.Wrapper.ArticleCollect;
import SAE.ApiREST.WebService.Wrapper.CollectionName;
import SAE.ApiREST.WebService.exception.ArticleException;
import SAE.ApiREST.WebService.exception.CollectException;
import SAE.ApiREST.WebService.model.Article;
import SAE.ApiREST.WebService.model.Collect;
import SAE.ApiREST.WebService.service.ICollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Controller
@RequestMapping("/CollectWebService")
public class CollectController {
    @Autowired
    ICollectionService collectionService;

    public CollectController() {

    }

    // region Collection

    // region GET
    @GetMapping(value = "/getAllCollection", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Collect> getAllCollection(){
        List<Collect> results = collectionService.getAllCollections();
        if(results.isEmpty()) {
            throw new CollectException("No collections available");
        }
        return results;
    }
    @GetMapping(value = "/getCollectionById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody EntityModel<Collect> getCollectionById(@PathVariable(value = "id") Integer id){
        Collect results = collectionService.getCollectionById(id);
        if(results == null) {
            throw new CollectException("No collections available");
        }
        return EntityModel.of(results,
                linkTo(methodOn(CollectController.class).getCollectionById(id)).withSelfRel(),
                linkTo(methodOn(CollectController.class).getAllArticlesById(id)
                ).withRel("getAllArticlesById"),
                linkTo(methodOn(CollectController.class).deleteColletionById(id)
                ).withRel("deleteColletionById"),
                linkTo(methodOn(CollectController.class).deleteAllColletionByName(results.getName())
                ).withRel("deleteAllColletionByName"),
                linkTo(methodOn(CollectController.class).getAllCollection()
                ).withRel("getAllCollection"));
    }
    @GetMapping(value = "/getAllCollectionsByName/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody CollectionModel<EntityModel<Collect>> getAllCollectionsByName(@PathVariable(value = "name") String name){
        List<Collect> results = collectionService.getAllCollectionsByName(name);
        if(results.isEmpty()) {
            throw new CollectException("No collections available");
        }
        List<EntityModel<Collect>> collectModels = results.stream()
                .map(collect -> EntityModel.of(collect,
                linkTo(methodOn(CollectController.class).getAllCollectionsByName(name)).withSelfRel(),
                linkTo(methodOn(CollectController.class).getAllCollection()
                ).withRel("getAllCollection")))
                .collect(Collectors.toList());
        return CollectionModel.of(collectModels);
    }
    // endregion

    // region DELETE
    @DeleteMapping(value = "/deleteColletionById/{id}")
    public @ResponseBody EntityModel<Link> deleteColletionById(@PathVariable("id") Integer id){
        if(!collectionService.deleteColletionById(id)) {
            throw new CollectException("No collections available");
        }
        return EntityModel.of(linkTo(methodOn(CollectController.class).getAllCollection()
                ).withRel("getAllCollection"));
    }

    @DeleteMapping(value = "/deleteColletionByName/{name}")
    public @ResponseBody EntityModel<Link> deleteColletionByName(@PathVariable("name") String name){
        if(!collectionService.deleteColletionByName(name)) {
            throw new CollectException("No collections available");
        }
        return EntityModel.of(linkTo(methodOn(CollectController.class).getAllCollection()
                ).withRel("getAllCollection"));
    }

    @DeleteMapping(value = "/deleteAllColletionByName/{name}")
    public @ResponseBody EntityModel<Link> deleteAllColletionByName(@PathVariable("name") String name){
        if(!collectionService.deleteAllColletionByName(name)) {
            throw new CollectException("No collections available");
        }
        return EntityModel.of(linkTo(methodOn(CollectController.class).getAllCollection()
                ).withRel("getAllCollection"));
    }
    // endregion

    // region PUT
    @PutMapping(value = "/addCollection")
    public @ResponseBody EntityModel<Collect> addCollection(@RequestBody Collect collection){
        Collect results = collectionService.addCollection(collection);
        return EntityModel.of(results,
                linkTo(methodOn(CollectController.class).getCollectionById(results.getId())).withSelfRel(),
                linkTo(methodOn(CollectController.class).getAllArticlesById(results.getId())
                ).withRel("getAllArticlesById"),
                linkTo(methodOn(CollectController.class).deleteColletionById(results.getId())
                ).withRel("deleteColletionById"),
                linkTo(methodOn(CollectController.class).deleteAllColletionByName(results.getName())
                ).withRel("deleteAllColletionByName"),
                linkTo(methodOn(CollectController.class).getAllCollection()
                ).withRel("getAllCollection"));
    }

    @PutMapping(value = "/addCollections")
    public @ResponseBody CollectionModel<EntityModel<Collect>> addCollections(@RequestBody List<Collect> collections){
        List<Collect> results = collectionService.addCollections(collections);
        List<EntityModel<Collect>> collectModels = results.stream()
                .map(collect -> EntityModel.of(collect,
                        linkTo(methodOn(CollectController.class).getAllCollection()).withRel("getAllCollection")))
                .collect(Collectors.toList());
        return CollectionModel.of(collectModels);
    }
    // endregion

    // region POST
    @PostMapping(value="/modifyCollectionName", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody EntityModel<Collect> modifyCollectionName(@RequestBody CollectionName request) {
        String newName = request.getNewName();
        Collect collection = request.getCollection();

        if(collection == null) {
            throw new ArticleException("No articles available");
        }
        Collect results = collectionService.modifyCollectionName(collection, newName);

        // Assuming your modification logic here

        return EntityModel.of(results,
                linkTo(methodOn(CollectController.class).getCollectionById(results.getId())).withSelfRel(),
                linkTo(methodOn(CollectController.class).getAllArticlesById(results.getId())).withRel("getAllArticlesById"),
                linkTo(methodOn(CollectController.class).deleteColletionById(results.getId())).withRel("deleteColletionById"),
                linkTo(methodOn(CollectController.class).deleteAllColletionByName(results.getName())).withRel("deleteAllColletionByName"),
                linkTo(methodOn(CollectController.class).getAllCollection()).withRel("getAllCollection"));
    }


    @PostMapping(value="/modifyCollectionNameById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody EntityModel<Collect> modifyCollectionNameById(@PathVariable("id") Integer id, @RequestBody String name){
        Collect results = collectionService.modifyCollectionNameById(id,name);
        return EntityModel.of(results,
                linkTo(methodOn(CollectController.class).getCollectionById(id)).withSelfRel(),
                linkTo(methodOn(CollectController.class).getAllArticlesById(id)
                ).withRel("getAllArticlesById"),
                linkTo(methodOn(CollectController.class).deleteColletionById(id)
                ).withRel("deleteColletionById"),
                linkTo(methodOn(CollectController.class).deleteAllColletionByName(results.getName())
                ).withRel("deleteAllColletionByName"),
                linkTo(methodOn(CollectController.class).getAllCollection()
                ).withRel("getAllCollection"));
    }
    // endregion
    // endregion

    // region Article
    @GetMapping(value = "/getAllArticlesById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody CollectionModel<EntityModel<Article>> getAllArticlesById(@PathVariable(value = "id") Integer id){
        List<Article> results = collectionService.getAllArticlesById(id);
        if(results == null) {
            throw new ArticleException("No articles available");
        }
        List<EntityModel<Article>> collectModels = results.stream()
                .map(collect -> EntityModel.of(collect,
                        linkTo(methodOn(CollectController.class).getCollectionById(id)).withRel("getCollection"),
                        linkTo(methodOn(CollectController.class).getAllCollection()).withRel("getAllCollection")))
                .collect(Collectors.toList());
        return CollectionModel.of(collectModels);
    }

    @PutMapping(value = "/addArticle", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody EntityModel<Article> addArticle(@RequestBody ArticleCollect request) {
        Article article = request.getNewArticle();
        Collect collection = request.getCollection();

        if(article == null) {
            throw new ArticleException("Invalid article");
        }
        if(collection == null) {
            throw new CollectException("No articles available");
        }
        Collect results = collectionService.addArticle(collection,article);
        Article art = results.getAllArticles().get(results.getAllArticles().indexOf(article));

        return EntityModel.of(art,
                linkTo(methodOn(CollectController.class).getCollectionById(results.getId())).withSelfRel(),
                linkTo(methodOn(TeacherController.class).getTeachById(results.getTeacher().getId())).withSelfRel(),
                linkTo(methodOn(ArticleControler.class).getArticleById(art.getId())).withSelfRel(),
                linkTo(methodOn(CollectController.class).getAllArticlesById(results.getId())
                ).withRel("getAllArticlesById"),
                linkTo(methodOn(CollectController.class).deleteColletionById(results.getId())
                ).withRel("deleteColletionById"),
                linkTo(methodOn(CollectController.class).deleteAllColletionByName(results.getName())
                ).withRel("deleteAllColletionByName"),
                linkTo(methodOn(CollectController.class).getAllCollection()
                ).withRel("getAllCollection"));
    }

    @DeleteMapping(value = "/deleteArticle", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody EntityModel<Collect> deleteArticle(@RequestBody ArticleCollect request) {
        Article article = request.getNewArticle();
        Collect collection = request.getCollection();

        if(article == null) {
            throw new ArticleException("Invalid article");
        }
        if(collection == null) {
            throw new CollectException("No articles available");
        }
        Collect results = collectionService.deleteArticle(collection,article);
        return EntityModel.of(results,
                linkTo(methodOn(CollectController.class).getCollectionById(results.getId())).withSelfRel(),
                linkTo(methodOn(CollectController.class).getAllArticlesById(results.getId())
                ).withRel("getAllArticlesById"),
                linkTo(methodOn(CollectController.class).deleteColletionById(results.getId())
                ).withRel("deleteColletionById"),
                linkTo(methodOn(CollectController.class).deleteAllColletionByName(results.getName())
                ).withRel("deleteAllColletionByName"),
                linkTo(methodOn(CollectController.class).getAllCollection()
                ).withRel("getAllCollection"));
    }
    //  endregion
}
