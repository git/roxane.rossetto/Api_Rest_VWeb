package SAE.ApiREST.WebService;

public class Response {

    Integer id;
    String statusMessage;

    public Response() {}

    public Response(Integer id, String statusMessage) {
        this.id = id;
        this.statusMessage = statusMessage;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatusMessage() {
        return this.statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
