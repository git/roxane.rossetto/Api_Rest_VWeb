package SAE.ApiREST.WebService.Wrapper;

import SAE.ApiREST.WebService.model.Article;
import SAE.ApiREST.WebService.model.Collect;

public class ArticleCollect {
    private Collect collection;
    private Article article;

    public ArticleCollect() {}

    public ArticleCollect(Collect collection, Article article) {
        this.collection = collection;
        this.article = article;
    }

    public Collect getCollection() {
        return collection;
    }

    public Article getNewArticle() {
        return article;
    }

    // Setters if necessary
}
