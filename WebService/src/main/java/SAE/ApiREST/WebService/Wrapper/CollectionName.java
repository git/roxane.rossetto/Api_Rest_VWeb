package SAE.ApiREST.WebService.Wrapper;

import SAE.ApiREST.WebService.model.Collect;

public class CollectionName {
    private Collect collection;
    private String newName;

    public CollectionName() {}

    public CollectionName(Collect collection, String newName) {
        this.collection = collection;
        this.newName = newName;
    }

    public Collect getCollection() {
        return collection;
    }

    public String getNewName() {
        return newName;
    }

    // Setters if necessary
}
