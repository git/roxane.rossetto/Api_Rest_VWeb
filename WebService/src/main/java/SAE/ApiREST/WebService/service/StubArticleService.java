package SAE.ApiREST.WebService.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import SAE.ApiREST.WebService.Response;
import SAE.ApiREST.WebService.model.Article;

@Service
public class StubArticleService implements IArticleService {
    
    // region POST
    @Override
    public Response addArticle(Article article) {
        return new Response(
            article.getId(),
            "Article successfully created"
        );
    }
    // endregion

    // region PUT
    @Override
    public Article updateTitle(Article article, String title) {
        Article newArticle = article;
        newArticle.setTitle(title);
        return newArticle;
    }

    @Override
    public Article updateUrl(Article article, String url) {
        Article newArticle = article;
        newArticle.setUrl(url);
        return newArticle;
    }

    @Override
    public Article updateDatePublished(Article article, String datePublished) {
        Article newArticle = article;
        newArticle.setDatePublished(datePublished);
        return newArticle;
    }

    @Override
    public Article updateDateAdded(Article article, String dateAdded) {
        Article newArticle = article;
        newArticle.setDateAdded(dateAdded);
        return newArticle;
    }

    @Override
    public Article changeVisibility(Article article) {
        Article newArticle = article;
        newArticle.setVisibility(!article.getVisible());
        return newArticle;
    }

    @Override
    public Article updateType(Article article, Integer type) {
        Article newArticle = article;
        newArticle.setType(type);
        return newArticle;
    }
    // endregion

    // region GET
    @Override
    public List<Article> getAllArticles() {
        List<Article> articles = new ArrayList<>();

        articles.add(new Article(
            "toi", 
            "azezeaea", 
            LocalDate.now().minusMonths(1), 
            LocalDate.now().minusMonths(2), 
            true, 
            1)
            );
        articles.add(new Article(
            "moi", 
            "zaeaeaeazeza", 
            LocalDate.now().minusMonths(2), 
            LocalDate.now().minusMonths(3),
            false,
            1)
            );
        articles.add(new Article(
            "eux", 
            "erfdhdh", 
            LocalDate.now().minusMonths(3), 
            LocalDate.now().minusMonths(4), 
            true,
            1)
            );
        articles.add(new Article(
            "tout ceux qui le veulent", 
            "azersdfgg", 
            LocalDate.now().minusMonths(4), 
            LocalDate.now().minusMonths(5), 
            false, 
            2)
            );

        return articles;
    }

    @Override
    public Article getArticlesById(Integer id) {
        return new Article(
            "azeaeaze", 
            "azezeaea", 
            LocalDate.now().minusMonths(1), 
            LocalDate.now().minusMonths(2), 
            true, 
            1);
    }

    @Override
    public List<Article> getArticlesByTitle(String title) {
        List<Article> articles = new ArrayList<>();

        articles.add(new Article(
            title, 
            "azezeaea", 
            LocalDate.now().minusMonths(1), 
            LocalDate.now().minusMonths(2), 
            true, 
            1)
            );

        return articles;
    }

    @Override
    public List<Article> getArticlesByUrl(String url) {
        List<Article> articles = new ArrayList<>();

        articles.add(new Article(
            "title", 
            url, 
            LocalDate.now().minusMonths(1), 
            LocalDate.now().minusMonths(2), 
            true, 
            1)
            );

        return articles;
    }

    @Override
    public List<Article> getArticlesByType(Integer type) {
        List<Article> articles = new ArrayList<>();

        articles.add(new Article(
            "aeazeazeaz", 
            "azezeaea", 
            LocalDate.now().minusMonths(1), 
            LocalDate.now().minusMonths(2), 
            true, 
            type)
            );

        return articles;
    }

    @Override
    public List<Article> getVisibleArticles() {
        List<Article> articles = new ArrayList<>();

        articles.add(new Article(
            "toi", 
            "azezeaea", 
            LocalDate.now().minusMonths(1), 
            LocalDate.now().minusMonths(2), 
            true, 
            1)
            );

        return articles;
    }

    @Override
    public List<Article> getInvisibleArticles() {
        List<Article> articles = new ArrayList<>();

        articles.add(new Article(
            "toi", 
            "azezeaea", 
            LocalDate.now().minusMonths(1), 
            LocalDate.now().minusMonths(2), 
            false, 
            1)
            );

        return articles;
    }

    @Override
    public List<Article> getArticlesAddedBefore(String dateAdded) {
        List<Article> articles = new ArrayList<>();

        articles.add(new Article(
            "toi", 
            "azezeaea", 
            LocalDate.parse(dateAdded, DateTimeFormatter.ISO_DATE).minusMonths(1), 
            LocalDate.now().minusMonths(2), 
            true, 
            1)
            );

        return articles;
    }

    @Override
    public List<Article> getArticlesAddedAfter(String dateAdded) {
        List<Article> articles = new ArrayList<>();

        articles.add(new Article(
            "toi", 
            "azezeaea", 
            LocalDate.parse(dateAdded, DateTimeFormatter.ISO_DATE).plusMonths(1), 
            LocalDate.now().minusMonths(2), 
            true, 
            1)
            );

        return articles;
    }
    
    @Override
    public List<Article> getArticlesAddedBetween(String beginning, String end) {
        List<Article> articles = new ArrayList<>();

        articles.add(new Article(
            "toi", 
            "azezeaea", 
            LocalDate.parse(beginning, DateTimeFormatter.ISO_DATE), 
            LocalDate.now().minusMonths(2), 
            true, 
            1)
            );

        return articles;
    }

    @Override
    public List<Article> getArticlesPublishedBefore(String datePublished) {
        List<Article> articles = new ArrayList<>();

        articles.add(new Article(
            "toi", 
            "azezeaea", 
            LocalDate.now().minusMonths(1), 
            LocalDate.parse(datePublished, DateTimeFormatter.ISO_DATE).plusMonths(2), 
            true, 
            1)
            );

        return articles;
    }

    @Override
    public List<Article> getArticlesPublishedAfter(String datePublished) {
        List<Article> articles = new ArrayList<>();

        articles.add(new Article(
            "toi", 
            "azezeaea", 
            LocalDate.now().minusMonths(1), 
            LocalDate.parse(datePublished, DateTimeFormatter.ISO_DATE).plusMonths(2), 
            true, 
            1)
            );

        return articles;
    }

    @Override
    public List<Article> getArticlesPublishedBetween(String beginning, String end) {
        List<Article> articles = new ArrayList<>();

        articles.add(new Article(
            "toi", 
            "azezeaea", 
            LocalDate.now().minusMonths(1), 
            LocalDate.parse(end, DateTimeFormatter.ISO_DATE), 
            true, 
            1)
            );

        return articles;
    }
    // endregion

    // region DELETE
    @Override
    public Response deleteArticleFromId(Integer id) {
        return new Response(
            id,
            "Article successfully deleted"
        );
    }
    
    @Override
    public Response deleteArticleFromUrl(String url) {
        return new Response(
            1,
            "Article successfully deleted"
        );
    }
    
    @Override
    public Response deleteArticleFromType(Integer type) {
        return new Response(
            1,
            "Article successfully deleted"
        );
    }

    @Override
    public Response deleteArticleFromTitle(String title) {
        return new Response(
            1,
            "Article successfully deleted"
        );
    }

    @Override
    public Response deleteArticleAddedBefore(String url) {
        return new Response(
            1,
            "Article successfully deleted"
        );
    }

    @Override
    public Response deleteArticleAddedAfter(String url) {
        return new Response(
            1,
            "Article successfully deleted"
        );
    }

    @Override
    public Response deleteArticleAddedBetween(String beginning, String end) {
        return new Response(
            1,
            "Article successfully deleted"
        );
    }

    @Override
    public Response deleteArticlePublishedBefore(String url) {
        return new Response(
            1,
            "Article successfully deleted"
        );
    }

    @Override
    public Response deleteArticlePublishedAfter(String url) {
        return new Response(
            1,
            "Article successfully deleted"
        );
    }

    @Override
    public Response deleteArticlePublishedBetween(String beginning, String end) {
        return new Response(
            1,
            "Article successfully deleted"
        );
    }
    // endregion
}