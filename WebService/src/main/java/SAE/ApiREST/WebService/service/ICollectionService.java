package SAE.ApiREST.WebService.service;
import SAE.ApiREST.WebService.model.Collect;
import SAE.ApiREST.WebService.model.Article;

import java.util.List;

public interface ICollectionService{
    public List<Collect> getAllCollections();
    public Collect getCollectionById(Integer id);
    public List<Collect> getAllCollectionsByName(String name);
    public Collect getCollection(Collect collect);
    public boolean deleteColletionById(Integer id);
    public boolean deleteColletionByName(String name);
    public boolean deleteAllColletionByName(String name);
    public Collect addCollection(Collect collection);
    public List<Collect> addCollections(List<Collect> collection);
    public Collect modifyCollectionName(Collect collection, String name);
    public Collect modifyCollectionNameById(Integer id, String name);
    public List<Article> getAllArticlesById(Integer id);
    public Collect addArticle(Collect collection, Article article);
    public Collect deleteArticle(Collect collection, Article article);
}