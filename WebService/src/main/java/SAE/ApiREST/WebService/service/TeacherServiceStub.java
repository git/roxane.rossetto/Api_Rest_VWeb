package SAE.ApiREST.WebService.service;

import SAE.ApiREST.WebService.Response;
import SAE.ApiREST.WebService.exception.TeacherException;
import SAE.ApiREST.WebService.model.Teacher;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TeacherServiceStub implements ITeacherService {

    @Override
    public List<Teacher> getAllTeacher() {
        List<Teacher> allTeacher = new ArrayList<Teacher>();

        allTeacher.add(new Teacher(1, "12-01-2023", "aline.alipres@gmail.com", "MsGarconManque"));
        allTeacher.add(new Teacher(2, "20-08-2023", "Viviane.Delvecchio@gmail.com", "MmeMath"));

        return allTeacher;
    }



    @Override
    public Teacher getTeacherById(Integer id) {
        return new Teacher(id, "10-01-2021", "exemple.gmail.com", "testest");
    }

    @Override
    public Teacher getTeacherByUsername(String username) { return new Teacher(12, "30-08-2020", "dadadou@gmail.com", username); }

    @Override
    public Teacher getTeacherByMail(String mail) {
        return new Teacher(20, "24-12-2021", mail, "tructruc");
    }

    @Override
    public Teacher getTeacherByDate(String date) {
        return new Teacher(5, date, "doudouda@gmail.com", "username");
    }

    @Override
    public List<Teacher> addTeacher(Teacher t) {
        List<Teacher> lteach = new ArrayList<Teacher>();
        lteach.add(t);
        return lteach;
    }

    @Override
    public List<Teacher> deleteTeacher(Integer id) {
        List<Teacher> allTeacher = new ArrayList<Teacher>();

        allTeacher.add(new Teacher(1,"12-01-2023", "aline.alipres@gmail.com", "MsGarconManque"));
        allTeacher.add(new Teacher(2, "20-08-2023", "Viviane.Delvecchio@gmail.com", "MmeMath"));

        if(allTeacher.remove(getTeacherById(id))){
            return allTeacher;
        } else {
            throw new TeacherException(String.format("Teacher {id} isn't removed", id));
        }
    }

    public Response modifyUsername(Teacher t, String newUsername){
        t.setUsername(newUsername);
        return new Response(t.getId(),String.format("This user %s has changed username", t.getMail()));
    }
}
