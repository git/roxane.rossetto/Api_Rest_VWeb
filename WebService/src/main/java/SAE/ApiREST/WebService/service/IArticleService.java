package SAE.ApiREST.WebService.service;

import java.util.List;

import SAE.ApiREST.WebService.Response;
import SAE.ApiREST.WebService.model.Article;

public interface IArticleService {

    // region POST
    public Response addArticle(Article article);
    // endregion

    // region PUT
    public Article updateTitle(Article article, String title);
    public Article updateUrl(Article article, String url);
    public Article updateDatePublished(Article article, String datePublished);
    public Article updateDateAdded(Article article, String dateAdded);
    public Article changeVisibility(Article article);
    public Article updateType(Article article, Integer type);
    // endregion

    // region GET
    public List<Article> getAllArticles();
    Article getArticlesById(Integer id);
    public List<Article> getArticlesByTitle(String title);
    public List<Article> getArticlesByUrl(String url);
    public List<Article> getArticlesByType(Integer type);
    public List<Article> getVisibleArticles();
    public List<Article> getInvisibleArticles();
    public List<Article> getArticlesAddedBefore(String dateAdded);
    public List<Article> getArticlesAddedAfter(String dateAdded);
    public List<Article> getArticlesAddedBetween(String beginning, String end);
    public List<Article> getArticlesPublishedBefore(String datePublished);
    public List<Article> getArticlesPublishedAfter(String datePublished);
    public List<Article> getArticlesPublishedBetween(String beginning, String end);
    // endregion

    // region DELETE
    public Response deleteArticleFromId(Integer id);
    public Response deleteArticleFromTitle(String title);
    public Response deleteArticleFromUrl(String url);
    public Response deleteArticleFromType(Integer type);
    public Response deleteArticleAddedBefore(String date);
    public Response deleteArticleAddedAfter(String date);
    public Response deleteArticleAddedBetween(String beginning, String end);
    public Response deleteArticlePublishedBefore(String date);
    public Response deleteArticlePublishedAfter(String date);
    public Response deleteArticlePublishedBetween(String beginning, String end);
    // endregion
}
