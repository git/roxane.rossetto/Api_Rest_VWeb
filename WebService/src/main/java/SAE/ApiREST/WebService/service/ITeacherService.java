package SAE.ApiREST.WebService.service;

import SAE.ApiREST.WebService.Response;
import SAE.ApiREST.WebService.model.Teacher;

import java.util.List;

public interface ITeacherService {
    public List<Teacher> getAllTeacher();
    public Teacher getTeacherById(Integer id);
    public Teacher getTeacherByUsername(String username);
    public Teacher getTeacherByMail(String mail);
    public Teacher getTeacherByDate(String date);
    public List<Teacher> addTeacher(Teacher t);
    public List<Teacher> deleteTeacher(Integer id);
    public Response modifyUsername(Teacher t, String newUsername);
}
