package SAE.ApiREST.WebService.service;
import SAE.ApiREST.WebService.model.Collect;
import SAE.ApiREST.WebService.model.Teacher;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import SAE.ApiREST.WebService.model.Article;
import org.springframework.stereotype.Service;

@Service
public class StubCollectionService implements ICollectionService {
    private List<Collect> collects = new ArrayList<>();

    public StubCollectionService(){
        this.collects.add(new Collect("collect1",  new Teacher(1, "12-03-2023", "aline.alipres@gmail.com", "MsGarconManque"),0));

        Collect collection2 = new Collect("collect2",  new Teacher(1, "12-03-2023", "aline.alipres@gmail.com", "MsGarconManque"),1);
        collection2.addArticle(new Article("toi","azezeaea", LocalDate.now().minusMonths(1),LocalDate.now().minusMonths(2),true,1));
        this.collects.add(collection2);

        Collect collection3 = new Collect("collect3",  new Teacher(1, "12-03-2023", "aline.alipres@gmail.com", "MsGarconManque"),3);
        collection3.addArticle(new Article("toi","azezeaea",LocalDate.now().minusMonths(1),LocalDate.now().minusMonths(2),true,1));
        collection3.addArticle(new Article("toi","azezeaea",LocalDate.now().minusMonths(1),LocalDate.now().minusMonths(2),true,1));
        this.collects.add(collection3);
    }

    @Override
    public List<Collect> getAllCollections() {
        return collects;
    }

    // region Collection

    // region GET
    @Override
    public Collect getCollectionById(Integer id){
        try {
            return collects.get(id);
        }catch (Exception e){
            return null;
        }
    }

    @Override
    public List<Collect> getAllCollectionsByName(String name){
        ArrayList<Collect> repCollections = new ArrayList<Collect>();
        for(Collect collection : collects){
            if(Objects.equals(collection.getName(), name)) {
                repCollections.add(collection);
            }
        }
        return repCollections;
    }

    @Override
    public Collect getCollection(Collect collect){
        return getCollectionById(collect.getId());
    }
    // endregion

    // region DELETE
    @Override
    public boolean deleteColletionById(Integer id){
        Collect collection = getCollectionById(id);
        if (collection != null) {
            collects.remove(collection);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteColletionByName(String name){
        List<Collect> collectionsByName = getAllCollectionsByName(name);
        if (!collectionsByName.isEmpty()) {
            collects.remove(collectionsByName.get(0));
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteAllColletionByName(String name){
        List<Collect> collectionsByName = getAllCollectionsByName(name);
        if (!collectionsByName.isEmpty()) {
            collects.removeAll(collectionsByName);
            return true;
        }
        return false;
    }

    @Override
    public Collect addCollection(Collect collection) {
        collects.add(collection);
        return collects.get(collects.lastIndexOf(collection));
    }
    // endregion

    // region PUT
    @Override
    public List<Collect> addCollections(List<Collect> collections){
        collects.addAll(collections);
        return collects;
    }
    // endregion

    // region POST
    @Override
    public Collect modifyCollectionName(Collect collection, String name){
        Collect collect = getCollectionById(collection.getId());
        collect.setName(name);
        return collect;
    }

    @Override
    public Collect modifyCollectionNameById(Integer id, String name){
        Collect collect = getCollectionById(id);
        collect.setName(name);
        return collect;
    }
    // endregion
    // endregion

    // region Article
    @Override
    public List<Article> getAllArticlesById(Integer id){
        Collect collect = getCollectionById(id);
        List<Article> result = collect.getAllArticles();
        if(!result.isEmpty()){
            return result;
        }
        return null;
    }
    @Override
    public Collect addArticle(Collect collect, Article article){
        Collect collection = getCollectionById(collect.getId());
        collection.addArticle(article);
        return collection;
    }
    @Override
    public Collect deleteArticle(Collect collect, Article article){
        Collect collection = getCollectionById(collect.getId());
        collection.removeArticle(article);
        return collection;
    }
    //  endregion
}